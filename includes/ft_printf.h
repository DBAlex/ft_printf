/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/16 15:20:36 by aseo              #+#    #+#             */
/*   Updated: 2019/10/30 11:23:46 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include "libft.h"
# include <stdarg.h>
# include <stdlib.h>
# include <stdint.h>

typedef struct		s_param
{
	char			left;
	char			zero;
	char			type;
	char			isprec;
	int				length;
	int				tlength;
	int				precision;
	void			*var;
	char			*pos;
	char			*end;
	struct s_param	*next;
}					t_param;

typedef struct		s_ptr
{
	char			*rstr;
	char			*c_str;
	char			*c_rstr;
	char			*tmpptr;
	int				size;
	va_list			*ap;
	t_param			*begin;
}					t_ptr;


int					ft_printf(const char *str, ...);
t_param				*new_param(void);
t_param				*set_param(t_ptr *p, char **ptr);
void				printf_malloc(const char *str, t_ptr *p);
void				parse_params(const char *str, t_ptr *p);
void				parse_flags(char **ptr, t_param *param, t_ptr *p);
void				handle_digits(char **ptr, int *result, t_ptr *p);
void				set_specifier(t_ptr *p, t_param *param, char c);
void				specifier_iud(t_ptr *p, t_param *param, char c);
void				specifier_pxx(t_ptr *p, t_param *param, char c);
void				specifier_s(t_ptr *p, t_param *param);
void				specifier_c(t_ptr *p, t_param *param);
void				specifier_p(t_param *param);
void				specifier_percent(t_param *param, char c);
void				set_hexa(t_ptr *p, t_param *param, char c);
void				build_str(t_ptr *p);
void				build_format(t_param *param, t_ptr *p);
void				handle_neg(t_param *param, t_ptr *p);
void				handle_x(t_ptr *p, t_param *param);
void				handle_p(t_ptr *p, t_param *param);
void				handle_c(t_ptr *p, t_param *param);
void				handle_s(t_ptr *p, t_param *param);
void				handle_i(t_ptr *p, t_param *param);
void				write_i(char *ptr, long long nbr);
void				itoa_hex(char *str, char *base, t_ull s, t_param *param);
void				free_params(t_param *begin);
int					ptr_length(t_ull ptr);
int					diu_length(long long nb);

#endif
