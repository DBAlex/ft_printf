#include "ft_printf.h"
#include <libft.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

void writebits (const unsigned long v, int fd);

int main()
{
	char *string = "C'est le test du printf avec une chaine bcp trong logogogogogogogogogogogogogogogogogogogogogogo";
	printf("|%10.5d|\n", -372);
	ft_printf("|%10.5d|\n", -372);
	printf("|%010d|\n", -372);
	ft_printf("|%010d|\n", -372);
	// ft_printf("|%.5d|\n", -2372);
	// printf("|%-4d|\n", -2464);
	// ft_printf("|%-4d|\n", -2464);
	// printf("|%%p::[%010d]|\n", -8473);
	// ft_printf("|%%p::[%010d]|\n", -8473);
}

void writebits (const unsigned long v, int fd)
{
    if (!v)  { putchar ('0'); return; };

    size_t sz = sizeof v * CHAR_BIT;
    unsigned long rem = 0;

    while (sz--)
        if ((rem = v >> sz))
            write (fd, (rem & 1) ? "1" : "0", 1);
}

// 2147483647
// 12345000000
