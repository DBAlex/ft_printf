/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_specifiers.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 14:39:37 by aseo              #+#    #+#             */
/*   Updated: 2019/10/30 11:21:17 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	specifier_iud(t_ptr *p, t_param *param, char c)
{
	param->type = 1;
	param->var = (c == 'u') ? (void *)(intptr_t)va_arg(*(p->ap), unsigned) :
				(void *)(intptr_t)va_arg(*(p->ap), int);
	if (param->var == 0 && param->precision == 0 && param->isprec)
		return ;
	param->tlength = diu_length((t_ull)param->var);
	if (param->precision < param->tlength)
		param->precision = param->tlength;
	if (param->length < param->precision)
		param->length = param->precision;
	if ((param->length <= param->tlength || param->length <= param->precision)
	&& (long long)param->var < 0)
		param->length++;
}

void	specifier_s(t_ptr *p, t_param *param)
{
	static char	*snull = "(null)";

	param->type = 2;
	param->var = va_arg(*(p->ap), void *);
	if (param->var == NULL)
		param->var = snull;
	param->tlength = ft_strlen((const char *)param->var);
	if (param->isprec && param->tlength > param->precision)
		param->tlength = param->precision;
	if (param->tlength > param->length)
		param->length = param->tlength;
}

void	specifier_c(t_ptr *p, t_param *param)
{
	param->type = 3;
	param->var = (void *)(intptr_t)va_arg(*(p->ap), int);
	if (param->length == 0)
		param->length = 1;
}

void	specifier_p(t_param *param)
{
	if (!param->var && param->zero && !param->left)
	{
		param->left = 1;
		param->precision = param->length - 2;
	}
	if (param->tlength + 2 > param->length)
		param->length = param->tlength + 2;
	if (param->precision + 2 > param->length)
		param->length = param->precision + 2;
	param->type = 4;
}

void	specifier_pxx(t_ptr *p, t_param *param, char c)
{
	param->var = va_arg(*(p->ap), void *);
	param->tlength = (c == 'p') ?
		ptr_length((t_ull)param->var) : ptr_length((unsigned int)param->var);
	if (!param->var && !param->precision && param->isprec)
	{
		param->isprec = 2;
		param->tlength--;
	}
	if (c == 'p')
		specifier_p(param);
	else
		param->type = (c == 'x') ? 5 : 6;
	if (param->tlength > param->length)
		param->length = param->tlength;
	if (param->precision < param->tlength)
		param->precision = param->tlength;
	if (param->precision > param->length)
		param->length = param->precision;
}
