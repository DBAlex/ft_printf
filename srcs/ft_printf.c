/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/16 15:20:23 by aseo              #+#    #+#             */
/*   Updated: 2019/10/29 22:23:56 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf(const char *str, ...)
{
	va_list ap;
	t_ptr	p;

	if (!ft_strchr(str, '%'))
	{
		write(1, str, (p.size = ft_strlen(str)));
		return (p.size);
	}
	if (str[0] == '%' && str[1] == 0)
		return (0);
	va_start(ap, str);
	p.ap = &ap;
	p.size = 0;
	parse_params(str, &p);
	printf_malloc(str, &p);
	va_end(ap);
	build_str(&p);
	write(1, p.rstr, p.size);
	free_params(p.begin);
	free(p.rstr);
	return (p.size);
}

void	build_str(t_ptr *p)
{
	t_param	*param;

	param = p->begin;
	while (param)
	{
		p->c_rstr = ft_memacpy(p->c_rstr, p->c_str, param->pos);
		p->c_str = param->end;
		build_format(param, p);
		param = param->next;
	}
	ft_memcpy(p->c_rstr, p->c_str, p->size - (p->c_rstr - p->rstr));
}

void	build_format(t_param *param, t_ptr *p)
{
	if (param->type == 1)
		handle_i(p, param);
	else if (param->type == 2)
		handle_s(p, param);
	else if (param->type == 3)
		handle_c(p, param);
	else if (param->type == 4)
		handle_p(p, param);
	else if (param->type == 5 || param->type == 6)
		handle_x(p, param);
	p->c_rstr += param->length;
}

void	printf_malloc(const char *str, t_ptr *p)
{
	p->rstr = malloc(p->size);
	p->c_rstr = p->rstr;
	p->c_str = (char *)str;
}
