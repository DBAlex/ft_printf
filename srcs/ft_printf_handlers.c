/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_handlers.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 14:16:49 by aseo              #+#    #+#             */
/*   Updated: 2019/10/30 11:24:03 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	handle_i(t_ptr *p, t_param *param)
{
	char	fillchr;
	int		neg;

	if (param->length == 0)
		return ;
	neg = (long long)param->var < 0;
	if (param->left)
	{
		write_i(p->c_rstr + param->precision - 1 + neg, (t_ull)param->var);
		ft_memset(p->c_rstr + neg, '0', param->precision - param->tlength);
		ft_memset(p->c_rstr + param->precision + neg, ' ',
		param->length - param->precision);
	}
	else
	{
		fillchr = (param->zero == 1 && param->isprec == 0) ? '0' : ' ';
		write_i(p->c_rstr + param->length - 1, (t_ull)param->var);
		ft_memset(p->c_rstr + param->length - param->precision, '0',
		param->precision - param->tlength);
		ft_memset(p->c_rstr, fillchr, param->length - param->precision);
	}
	if (neg)
		handle_neg(param, p);
}

void	handle_c(t_ptr *p, t_param *param)
{
	if (param->left)
		*(p->c_rstr) = (char)(intptr_t)param->var;
	else
		*(p->c_rstr + param->length - 1) = (char)(intptr_t)param->var;
	ft_memset(p->c_rstr + param->left,
			(param->zero && !param->left) ? '0' : ' ', param->length - 1);
}

void	handle_s(t_ptr *p, t_param *param)
{
	int		outlen;
	char	fillchr;

	if (!param->isprec)
		outlen = param->tlength;
	else
		outlen = (param->precision < param->tlength) ?
				param->precision : param->tlength;
	fillchr = (param->zero == 1) ? '0' : ' ';
	if (param->left == 0)
	{
		ft_memcpy(p->c_rstr + param->length - outlen,
				(const char *)param->var, outlen);
		ft_memset(p->c_rstr, fillchr, param->length - outlen);
	}
	else
	{
		ft_memcpy(p->c_rstr, (const char *)param->var, outlen);
		ft_memset(p->c_rstr + outlen, ' ', param->length - outlen);
	}
}

void	handle_p(t_ptr *p, t_param *param)
{
	static char *lower = "0123456789abcdef";

	if (param->left == 0)
	{
		itoa_hex(p->c_rstr + param->length - param->tlength, lower,
				(t_ull)param->var, param);
		ft_memset(p->c_rstr + param->length - param->precision, '0',
				param->precision - param->tlength);
		ft_memset(p->c_rstr, ' ', param->length - param->precision - 2);
		*(p->c_rstr + param->length - param->precision - 2) = '0';
		*(p->c_rstr + param->length - param->precision - 1) = 'x';
	}
	else
	{
		*(p->c_rstr) = '0';
		*(p->c_rstr + 1) = 'x';
		ft_memset(p->c_rstr + 2, '0', param->precision - param->tlength);
		itoa_hex(p->c_rstr + param->precision - param->tlength + 2, lower,
				(t_ull)param->var, param);
		ft_memset(p->c_rstr + param->precision + 2, ' ',
				param->length - param->precision - 2);
	}
}

void	handle_x(t_ptr *p, t_param *param)
{
	static char		*lower = "0123456789abcdef";
	static char		*upper = "0123456789ABCDEF";
	char			*base;
	unsigned int	holder;

	base = (param->type == 5) ? lower : upper;
	holder = (unsigned int)param->var;
	if (param->left == 0)
	{
		itoa_hex(p->c_rstr + param->length - param->tlength, base,
				holder, param);
		ft_memset(p->c_rstr + param->length - param->precision, '0',
				param->precision - param->tlength);
		ft_memset(p->c_rstr, (param->zero && !param->isprec) ? '0' : ' ',
				param->length - param->precision);
	}
	else
	{
		ft_memset(p->c_rstr, '0', param->precision - param->tlength);
		itoa_hex(p->c_rstr + param->precision - param->tlength, base,
				holder, param);
		ft_memset(p->c_rstr + param->precision, ' ',
				param->length - param->precision);
	}
}
