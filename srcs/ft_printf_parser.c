/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_parser.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 14:29:39 by aseo              #+#    #+#             */
/*   Updated: 2019/10/29 17:45:43 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	parse_params(const char *str, t_ptr *p)
{
	char	*ptr;
	t_param	*param;
	t_param	*tparam;

	ptr = (char *)str;
	p->tmpptr = (char *)str;
	p->begin = 0;
	param = 0;
	while (*ptr)
		if (*ptr != '%')
			ptr++;
		else if ((tparam = set_param(p, &ptr)))
		{
			if (param)
			{
				param->next = tparam;
				param = param->next;
			}
			else
			{
				param = tparam;
				p->begin = param;
			}
		}
	p->size += ptr - p->tmpptr;
}

t_param	*set_param(t_ptr *p, char **ptr)
{
	t_param	*param;

	param = new_param();
	p->size += *ptr - p->tmpptr;
	param->pos = (*ptr)++;
	parse_flags(ptr, param, p);
	set_specifier(p, param, **ptr);
	if (param->type == -1)
	{
		free(param);
		return (NULL);
	}
	p->size += param->length;
	param->end = ++(*ptr);
	p->tmpptr = *ptr;
	return (param);
}

void	parse_flags(char **ptr, t_param *param, t_ptr *p)
{
	while (**ptr == '0' || **ptr == '-')
	{
		if (**ptr == '0')
			param->zero = 1;
		if (**ptr == '-')
			param->left = 1;
		(*ptr)++;
	}
	handle_digits(ptr, &(param->length), p);
	if (**ptr == '.')
	{
		(*ptr)++;
		handle_digits(ptr, &(param->precision), p);
		param->isprec = 1;
	}
	if (param->length < 0)
	{
		param->length = -(param->length);
		param->left = 1;
	}
}

void	set_specifier(t_ptr *p, t_param *param, char c)
{
	if (c == 'i' || c == 'u' || c == 'd')
		specifier_iud(p, param, c);
	else if (c == 's')
		specifier_s(p, param);
	else if (c == 'c')
		specifier_c(p, param);
	else if (c == 'p' || c == 'x' || c == 'X')
		specifier_pxx(p, param, c);
	else if (c == '%' || ft_isalpha(c))
		specifier_percent(param, c);
	else
		param->type = -1;
}
