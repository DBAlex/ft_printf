/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_handlers_utils.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 14:25:26 by aseo              #+#    #+#             */
/*   Updated: 2019/10/30 11:23:19 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	write_i(char *ptr, long long nbr)
{
	t_ull	nb;

	nb = (nbr < 0) ? -nbr : nbr;
	while (nb > 9)
	{
		*ptr-- = (nb % 10) + 48;
		nb /= 10;
	}
	*ptr-- = nb + 48;
}

void	itoa_hex(char *str, char *base, t_ull s, t_param *param)
{
	t_ull	ptr;
	t_ull	mask;
	int		i;
	int		show;

	(void)param;
	mask = 0xF00000000000;
	i = 44;
	show = 0;
	while (i >= 0)
	{
		ptr = ((t_ull)s & mask) >> i;
		if (ptr)
			show = 1;
		if (show || (!i && param->isprec != 2))
			*str++ = base[ptr];
		i -= 4;
		mask = mask >> 4;
	}
}

void	handle_neg(t_param *param, t_ptr *p)
{
	if ((param->zero && !param->isprec) || param->left)
		*(p->c_rstr) = '-';
	else
		*(p->c_rstr + param->length - param->precision - 1) = '-';
}
