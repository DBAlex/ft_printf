/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_specifiers2.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/29 16:43:27 by aseo              #+#    #+#             */
/*   Updated: 2019/10/29 16:48:18 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	specifier_percent(t_param *param, char c)
{
	param->type = 3;
	param->var = (void *)(intptr_t)c;
	if (param->length == 0)
		param->length = 1;
}
