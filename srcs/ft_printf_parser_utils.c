/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_parser_utils.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 15:11:51 by aseo              #+#    #+#             */
/*   Updated: 2019/10/29 20:42:32 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		diu_length(long long nb)
{
	int		i;

	i = 0;
	if (nb < 0)
	{
		nb = -nb;
	}
	while (nb > 9)
	{
		nb /= 10;
		i++;
	}
	return (i + 1);
}

int		ptr_length(t_ull ptr)
{
	t_ull	p;
	int		i;

	i = 0;
	p = ptr;
	while (p > 15)
	{
		p /= 16;
		i++;
	}
	return (i + 1);
}

void	handle_digits(char **ptr, int *result, t_ptr *p)
{
	if (**ptr == '*')
	{
		*result = va_arg(*(p->ap), int);
		(*ptr)++;
	}
	while (ft_isdigit(**ptr))
	{
		*result = *result * 10 + **ptr - 48;
		(*ptr)++;
	}
}
