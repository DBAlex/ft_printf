/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aseo <aseo@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 14:31:22 by aseo              #+#    #+#             */
/*   Updated: 2019/10/29 22:10:18 by aseo             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_param	*new_param(void)
{
	t_param *ptr;

	ptr = malloc(sizeof(t_param));
	ptr->length = 0;
	ptr->tlength = 0;
	ptr->left = 0;
	ptr->zero = 0;
	ptr->next = 0;
	ptr->isprec = 0;
	ptr->precision = 0;
	return (ptr);
}

void	free_params(t_param *begin)
{
	t_param *ptr;
	t_param *tmpptr;

	ptr = begin;
	while (ptr)
	{
		tmpptr = ptr->next;
		free(ptr);
		ptr = tmpptr;
	}
}
