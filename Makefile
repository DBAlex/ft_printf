NAME		= libftprintf.a

CC			= gcc

CFLAGS		= -Wall -Wextra -Werror -g


SRCDIR		= srcs
OBJDIR		= objs

SOURCES		:= $(wildcard $(SRCDIR)/*.c)
OBJECTS		:= $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

rm			= rm -f


all:		$(NAME)

$(NAME):	$(OBJECTS)
			cp /usr/lib/libft.a $@
			ar rs $@ $(OBJECTS)
			ranlib $@

$(OBJECTS):	$(OBJDIR)/%.o : $(SRCDIR)/%.c
			$(CC) $(CFLAGS) -c $< -o $@ -I includes

clean:
			$(RM) $(OBJECTS)

fclean:		clean
			$(RM) $(NAME)

re:			fclean all

so:			$(OBJECTS)
			$(CC) $(CFLAGS) $(OBJECTS) -shared -o libft.so

.PHONY:		all clean fclean re
